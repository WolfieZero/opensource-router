/**
 * Makes the page height fluid to browser heigh when called
 * @return {object} Returns the current dimension of elements
 */
fixFluidMapperHeight = function() {

	var	clientHeight    = document.body.clientHeight,
        clientWidth     = document.body.clientWidth,
		headerHeight    = $('#header').height(),
		appContainer    = $('article'),
		routeContainer  = $('aside'),
        navContainer    = routeContainer.find('nav').height(),
        mapContainer    = $('#map'),
        routeWidth      = routeContainer.width(),
        dimension       = {
            container: {
                width: clientWidth,
                height: clientHeight - headerHeight
            },
            route: {
                height: clientHeight - headerHeight - navContainer - 20
            },
            map: {
                width: clientWidth - routeWidth - 1
            }
        };

    appContainer.width(dimension.container.width);
    appContainer.height(dimension.container.height);
    routeContainer.find('.panel').height(dimension.route.height);
    mapContainer.width(dimension.map.width);

    return dimension;

};


/*****************************************************************************/


$(document).ready(function() {
	
	// Just in case JS is disabled we can style our page accordingly
	$('html').removeClass('no-js').addClass('yes-js');

    // Makes the app fluid to the browser size
	fixFluidMapperHeight();

    // Load in the Router class and set it globally for debugging
    Router.init();

});
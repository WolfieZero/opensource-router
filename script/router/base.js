/**
 * Router Base
 * ============================================================================
 *
 * @author Neil Sweeney
 * @since  0.2
 *
 *
 * License
 * ----------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU AFFERO General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA or see
 * http://www.gnu.org/licenses/agpl.txt.
 */



/**
 * The main object for Router. To use Router you will need to call this object
 * with the `init()` function: `Router.init()`
 * @type {Object}
 */
var Router = {

    version: 0.2,

    /**
     * Default value for MPG
     * @type {Number}
     */
    _mpg: 37.8,

    /**
     * Defauly value for the cost of fuel per litre
     * @type {Number}
     */
    _avgCostPerLitre: 145.9,

    /**
     * Maneuver marker
     * @type {L}
     */
    _popup: new L.Popup({autoPan: false}),

    /**
     * Is the maneuver marker open or closed?
     * @type {Boolean}
     */
    _isPopupOpen: false,


    init: function() {

        // Show the map
        Router.Map.display();
        
        // Show the menu arrow
        Router.Menu.arrow();

        // Start listening for events
        Router.Listener();

    },


    staticListener: function() {

        // Change the menu view
        $('#view-directions').on('click', function(e) {
            self.menuSelect('directions');
        });
        $('#view-plan').on('click', function(e) {
            self.menuSelect('route');
        });

    },

    dynamicListener: function() {

    }

};
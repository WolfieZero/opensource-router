Router.Menu = {

    /**
     * Menu container
     * @type {jQuery}
     */
    containerItem: $(Router.CONSTANT.dom.menu.container),

    /**
     * Plan item in menu
     * @type {jQuery}
     */
    planItem: $(Router.CONSTANT.dom.menu.plan),
    
    /**
     * Direcitons item in menu
     * @type {jQuery}
     */
    directionsItem: $(Router.CONSTANT.dom.menu.directions),

    /**
     * Arrow manipulation. If `moveby` is defined then it will move i.arrow by
     * that value, otherwise it adds the i.arrow to the menu.
     * @param  {Number} moveby Relative value to position arrow in the menu
     * @return {Boolean}       If the action was successful
     */
    arrow: function(moveby) {

        if (moveby === undefined) {

            Router.Menu.containerItem.append('<i class="arrow"></i>');
       //     viewMenu.find('.arrow').css('left', '65px').css('z-index', '0');

            return true;

        } else {

            Router.Menu.containerItem.find('.arrow').animate({
                left: moveby
            }, Router.Menu.CONSTANT.menu.arrowDuration);

            return true;

        }

        return false;
    }

};
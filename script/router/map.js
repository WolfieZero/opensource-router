/**
 * Router Map
 * ============================================================================
 *
 * @author Neil Sweeney
 * @since  0.2
 *
 *
 * License
 * ----------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU AFFERO General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA or see
 * http://www.gnu.org/licenses/agpl.txt.
 */



/**
 * All map functionaility is dealt with via this object. It requires Leaflet to
 * be requiested to work. Initention here is to replace this file with a custom
 * one so you can use any mapping API you wish and not just Leaflet
 * @type {Object}
 */
Router.Map = {

    /**
     * Leaflet map object
     * @type {Object}
     */
    _map: {},

    /**
     * Markers on map
     * @type {Array}
     */
    _markers: [],


    /**
     * Show the map on the site.
     * @return {[type]} [description]
     */
    display: function() {

        Router.Map._map = L.map(Router.CONSTANT.map.element).setView(Router.CONSTANT.map.view, Router.CONSTANT.map.zoom);

        L.tileLayer(Router.CONSTANT.map.tileURL, {
            attribution: Router.CONSTANT.map.attribution,
            subdomains: Router.CONSTANT.map.tileDomains,
            maxZoom: Router.CONSTANT.map.maxZoom
        }).addTo(Router.Map._map);

    },

    /**
     * Adds a marker to the Map
     * @param {Object} latlng  The LatLng object to place the marker
     * @param {String} content HTML contents of the marker
     * @param {Object} options Options for the marker
     */
    addMarker: function(latlng, content, options) {
        if (options === undefined) options = {};

        var self    = this,
            marker  = new L.Marker(latlng, options);

        if (content !== undefined || content !== null) {
            marker.bindPopup(content, Router.CONSTANT.popup);
        }

        self._map.addLayer(marker);
        self._markers.push(marker);
    }

};
/**
 * Router Geocode
 * ============================================================================
 *
 * @author Neil Sweeney
 * @since  0.2
 *
 *
 * License
 * ----------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU AFFERO General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA or see
 * http://www.gnu.org/licenses/agpl.txt.
 */


/**
 * Geocoding points to be used on the map. This relys on Nominatim Search
 * Service API - http://open.mapquestapi.com/nominatim/
 * @type {Object}
 */
Router.Geocode = {

    /**
     * Takes the locale given then passes it to an API to get back geocoidng
     * information.
     * @param  {String} locale Location that we want to find map info for
     * @return {Object}
     */
    reverse: function(locale) {

        var jsonURL     = 'http://open.mapquestapi.com/nominatim/v1/search?format=json' +
                          '&json_callback=?' +
                          '&q=' + escape(locale) +
                          '&addressdetails=1' +
                          '&limit=' + Router.config.revGeocode.limit +
                          '&countrycodes=gb' +
                          '',

            revGeocode  = $.ajax({
                    url:    jsonURL,
                    async:  false }),

            returnObj   = {};

        if (revGeocode.responseText === '[]') {
            returnObj = {error: 'No location found'};
        } else {
            returnObj = $.parseJSON(revGeocode.responseText);
        }

        return returnObj;

    }

};
/**
 * Router JSON
 * ============================================================================
 *
 * @author Neil Sweeney
 * @since  0.2
 *
 * Code based on Stegan Lange-Hegermann's MicroAJAX, that can be found at
 * http://code.google.com/p/microajax/, but has since been modified
 */


Router.JSON = {

    postBody: null,

    /**
     * URL of the JSON feed
     * @type {String}
     */
    _url: '',

    /**
     * Callback function to do
     * @type {Function}
     */
    _callback: {},

    _request: {},

    /**
     * Grabs and parses the JSON in the URL
     * @param  {String}   url      Address of JSON file
     * @param  {Function} callback [description]
     * @return {[type]}            [description]
     */
    grab: function(url, callback) {

        this.postBody = (arguments[2] || '');

        this._url       = url;
        this._callback  = callback;
        this._request   = this.getRequest();

        if (this._request) {

            var req = this._request;
            req.onreadystatechange = this.bindFunction(this.stateChange, this);

            if (this.postBody !== '') {
                req.open('POST', url, true);
                req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                req.setRequestHeader('Connection', 'close');
            } else {
                req.open('GET', url, true);
            }

            req.send(this.postBody);

        }

    },

    bindFunction: function (caller, object) {

        return function() {
            return caller.apply(object, [object]);
        };

    },

    stateChange: function (object) {

        if (this._request.readyState === 4)
            this._callback(this._request.responseText);

    },


    getRequest: function() {

        if (window.ActiveXObject)
            return new ActiveXObject('Microsoft.XMLHTTP');

        else if (window.XMLHttpRequest)
            return new XMLHttpRequest();

        return false;

    }

};
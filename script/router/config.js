/**
 * Router configuration; these are set by the developer and do not change.
 * They should be treated as constants.
 * @type {Object}
 */
Router.CONSTANT = {

    /**
     * DOM
     * @type {Object}
     */
    dom: {
        plan: '#router-plan',
        directions: '#router-directions',
        menu: {
            container: '#router-nav',
            plan: '#view-plan',
            directions: '#view-directions'
        }
    },

    /**
     * Map
     * @type {Object}
     */
    map: {
        element: 'map', // Must be an ID
        tileURL: 'http://{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png',
        tileDomains: ['otile1','otile2','otile3','otile4'],
        attribution: 'Data, imagery and map information provided by <a href="http://open.mapquest.co.uk" target="_blank">MapQuest</a>, <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> and contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/" target="_blank">CC-BY-SA</a>',
        maxZoom: 18,
        zoom: 6,
        view: [54.2, -4]
    },

    /**
     * [menu description]
     * @type {Object}
     */
    menu: {
        container: 'router-nav',    // ID
        arrowDuration: 500,
        arrowPos: {
            plan: 65,
            directions: 215
        }
    },

    /**
     * Convertion metrics used as 1:*
     * @type {Object}
     */
    metric: {
        geosToMile: 0.0145,
        litresToGallon: 4.54609
    },

    /**
     * Navigation
     * @type {Object}
     */
    navigation: {
        unit: 'm',
        narrativeType: 'microformat',
        enhancedNarrative: 'true',
        routeType: 'fastest',
        generalize: 1,
        drivingStyle: 2,
        locale: 'en_GB'
    },

    /**
     * Reverse geocoding
     * @type {Object}
     */
    revGeocode: {
        limit: 1
    },

    /**
     * Route shaping
     * @type {Object}
     */
    routeShape: {
        color: '#C01',
        clickable: true,
        opacity: 0.7
    },

    /**
     * In map popup box
     * @type {Object}
     */
    popup: {
        maxWidth: 600
    }

};
var Router = {

    /**
     * Default value for MPG
     * @type {Number}
     */
    _mpg: 37.8,

    /**
     * Defauly value for the cost of fuel per litre
     * @type {Number}
     */
    _avgCostPerLitre: 145.9,

    /**
     * Maneuver marker
     * @type {L}
     */
    _popup: new L.Popup({autoPan: false}),

    /**
     * Is the maneuver marker open or closed?
     * @type {Boolean}
     */
    _isPopupOpen: false,

    /**
     * Markers on map
     * @type {Array}
     */
    _markers: [],

    /**
     * Leaflet map object
     * @type {Object}
     */
    _map: {}

};

Router.init = function() {

    ////////////////////
    // Init functions //
    ////////////////////

    // Show the map
    this.displayMap();
    
    // Show the menu arrow
    this.menuArrow();

    // Start listening for events
    this.listener();

};


/*****************************************************************************/


/**
 * Listens out for certain events to happen
 * @return {null}
 */
Router.listener = function() {

	var self = this;


	// Resize the page when the window changes
	window.onresize = function() {
		fixFluidMapperHeight();
	};


	// Process the route
	$('#router-plan').on('submit', function(e) {
		e.preventDefault();
		self.routeSubmit();
	});


    // Change the menu view
    $('#view-directions').on('click', function(e) {
        self.menuSelect('directions');
    });
    $('#view-plan').on('click', function(e) {
        self.menuSelect('route');
    });

};


/*****************************************************************************/


/**
 * Display the map
 * @return {null}
 */
Router.displayMap = function() {

	var self = this;

	self._map = L.map('map').setView(Router.config.map.defaultView, Router.config.map.defaultZoom);

    L.tileLayer(Router.config.map.tileURL, {
        attribution: Router.config.map.attribution,
        subdomains: Router.config.map.tileDomains,
        maxZoom: 18
    }).addTo(self._map);

};


/*****************************************************************************/


/**
 * Does a reverse geocode thanks to Open MapQuest's Nominatim service.
 * @param  {String} locale Postcode or placename
 * @return {Object} Returns the JSON repsonse from Nominatim service
 */
Router.revGeocode = function(locale) {

	var self		= this,
		jsonURL		= 'http://open.mapquestapi.com/nominatim/v1/search?format=json' +
						'&json_callback=?' +
						'&q=' + escape(locale) +
						'&addressdetails=1' +
						'&limit=' + Router.config.revGeocode.limit +
						'&countrycodes=gb' +
						'',
		revGeocode	= $.ajax({
				url:	jsonURL,
				async:	false
			});

	if (revGeocode.responseText === '[]') {
		revGeocode.responseText = '{"error":"No location found"}';
	}

	return $.parseJSON(revGeocode.responseText);
};


/*****************************************************************************/


/**
 * User has submitted a route plan and we need to check through it
 * @param  {String} start The start point
 * @param  {String} end The end point
 * @param  {String} routetype Type of routing wanted
 * @return {Boolean} True if successful, otherwise false
 */
Router.routeSubmit = function(start, end, routetype) {

	var self = this;


	// Setup the variables
	if (start === undefined)		start		= $('input[name=a]').val();
	if (end === undefined)			end			= $('input[name=b]').val();
	if (routetype === undefined)	routetype	= $('input[name=r]:checked').val();

	// Pass to set methods
	self.setStartLoc(start);
	self.setEndLoc(end);
	self.setRouteType(routetype);

	// Get the values and assign them locally
	start		= self.getStartLoc();		// Start of route
	end			= self.getEndLoc();			// End of route
	routetype	= self.getRouteType();		// Type if routing wanted (fast or short)

	// Found an issue with the start location
	if (start.error) {
		dialog.alert(
			'<strong>Error with start location</strong><br />' + start.error,
			function() {
				$('input[name=start]').focus();
			}
		);
		return false;
	}

	// Found an issue with the end location
	if (end.error) {
		dialog.alert(
			'<strong>Error with end location</strong><br />' + end.error,
			function() {
				$('input[name=end]').focus();
			}
		);
		return false;
	}

	
	// Check the user is happy to continue with their current selection
	var	message = 'You are looking for the '+routetype+' route from <strong>'+start.name+' to '+end.name+'</strong>.'+
					'<br />Do you want to carry on with this selection?';

	self.routeProcess();

	return true;

};


/*****************************************************************************/


/**
 * Process Route
 * Show the route on the map along with fuel stations
 * @return {Boolean} Returns true if successful, otherwise false
 */
Router.routeProcess = function() {

	var	self		= this,

		start		= self.getStartLoc(),
		end			= self.getEndLoc(),
		routetype	= self.getRouteType(),

		startLatLng	= {
			lat:	start.data[0].lat,
			lng:	start.data[0].lon },
		endLatLng	= {
			lat:	end.data[0].lat,
			lng:	start.data[0].lon },

		queryPart	=	'?a='+escape(start.name.replace(' ', '+'))+	// URL safe start of route
						'&b='+escape(end.name.replace(' ', '+'))+	// URL safe end of route
						'&r='+routetype;


	// Update the URL so it's linkable
	history.pushState(
		{
			start:	start,
			end:	end
		},
		'Directions from '+start.name+' to '+end.name+' along the '+routetype+' route',
		queryPart
	);


	// Add the route to the map first then if successful grab the fueling stations
	self.navigation(start, end, routetype, function(data) {

		if (Router._markers.length > 0) {
			for (var i in Router._markers) {
				Router._map.removeLayer(self._markers[i]);
			}
		}

		// Assign the directions to the class
		self.setDirections(data);

		// Add the start and end points to the map
		self.mapAddPointMarker(start, 'a');
		self.mapAddPointMarker(end, 'b');

		// Process the routing information
		self.mapAddRouteShape();

		// Fit the map to the bounds of the route
		self._map.fitBounds(new L.LatLngBounds(
			new L.LatLng(data.route.boundingBox.lr.lat, data.route.boundingBox.lr.lng),
			new L.LatLng(data.route.boundingBox.ul.lat, data.route.boundingBox.ul.lng)
		));
			
		// Add the maneuvers to the aside
		self.displayManeuvers();
			
		// Change the menu selector to show the maneuvers (needs to be last visual part)
		self.menuSelect('directions');
			
		// Display the maneuver when user hovers over it and focus when clicked
        // Cannot be added to the listener as it requires to be refreshed
        $('.maneuver').on({
            click: function() {
                self.mapDisplayManeuver(
                    $(this).data('maneuver'),
                    'click'
                );
            },
            mouseenter: function() {
                self.mapDisplayManeuver(
                    $(this).data('maneuver'),
                    'hover'
                );
            }
        });

	});
};


/*****************************************************************************/


/**
 * Display Maneuvers
 * Builds the HTML for directions to show the maneuvers
 */
Router.displayManeuvers = function() {

    var self            = this,
        directions      = self.getDirections(),
        html            = tmpl('tmpl_maneuvers', {
            start:              self.getStartLoc(),
            end:                self.getEndLoc(),
            maneuvers:          directions.route.legs[0].maneuvers,
            cost:               self.journeyCost(),
            time:               directions.route.formattedTime,
            distance:           directions.route.distance,
            copyright:          directions.info.copyright.text });
    $(Router.config.element.directions).html(html);

};


/*****************************************************************************/


/**
 * Add Route Shape
 * Gets the directions from the private variable and adds it to the map
 */
Router.mapAddRouteShape = function() {

    var self        = this,
        directions  = self.getDirections();

    if (window.routing !== undefined) self._map.removeLayer(window.routing);

    window.routing = new L.Polyline(directions.shape.object, Router.config.routeShape);
    self._map.addLayer(window.routing);

};


/*****************************************************************************/


/**
 * Display Maneuver on Map
 * Takes the index of a maneuver and then adds a popup to the map
 * @param  {Number} i The index of the maneuver
 * @param  {String} state Choice: hover|click
 */
Router.mapDisplayManeuver = function(i, state) {

    var self        = this,
        directions  = self.getDirections(),
        maneuver    = directions.route.legs[0].maneuvers[i],
        latlng      = new L.LatLng(maneuver.startPoint.lat, maneuver.startPoint.lng),
        content     = tmpl('tmpl_maneuver', {
            i:          parseInt(i, 10)+1,
            maneuver:   maneuver.narrative });

    self._popup
        .setLatLng(latlng)
        .setContent(content);

    // We need to check if it's open so we don't get transition effects
    if (self._isPopupOpen === false) {
        self._map.openPopup(self._popup);
        self._isPopupOpen = true;   // We have a listener to set this `false` if it is closed
    }

    if (state === 'click') {
        self._map.panTo(latlng);
    } else
    if (state === 'hover') {

    }
};


/*****************************************************************************/


/**
 * Add Point Maker
 * Use to add the start and end points
 * @param  {object} point The point that we wish to add
 * @param  {char} pos If it's `a` or `b`
 */
Router.mapAddPointMarker = function(point, pos) {

	var self		= this,
		location	= new L.LatLng(point.data[0].lat, point.data[0].lon),
		content		= tmpl('tmpl_point', {address: point.name});
		/*
		buildIcon	= L.Icon.extend({options: {
			iconUrl:	routeplanner_plugin_url+'/img/'+pos+'.png',
			shadowUrl:	null,
			iconSize:	new L.Point(31,44),
			iconAnchor:	new L.Point(16,41)
		}}),
		icon		= new buildIcon();
	 	 */
//	self.mapAddMarker(location, content, {icon: icon});
	self.mapAddMarker(location, content);

};


/*****************************************************************************/


Router.mapAddMarker = function(latlng, content, options) {

	if (options === undefined) options = {};

	var	self	= this,
		marker	= new L.Marker(latlng, options);

	if (content !== undefined || content !== null) {
		marker.bindPopup(content, Router.config.popup);
	}

	self._map.addLayer(marker);
	self._markers.push(marker);

};


/*****************************************************************************/


/**
 * Working out the total cost of journey based on a few assumptions
 * @return {Number} Cost of the journey
 */
Router.journeyCost = function() {

    var self        = this,
        
        directions  = self.getDirections(),
        mpg         = self.getMPG(),
        avgCost     = self.getAvgCostPerLitre(),

        mpl         = mpg / self.config.metric.litresToGallon,
        litersReq   = directions.route.distance / mpl,
        finalCost   = parseFloat((litersReq * avgCost) / 100).toFixed(2);

    return finalCost;
};


/*****************************************************************************/


/**
 * Navigation
 *
 * @param  {String} start The starting point
 * @param  {String} end The end point
 * @param  {Function} callback The function we want to run at the end
 * @return {Boolean} False if the script fails to run
 */
Router.navigation = function(start, end, routetype, callback) {

	// Build up data on our route
	var	self		= this,
		jsonURL		= 'http://open.mapquestapi.com/directions/v0/route?outFormat=json&callback=?'+
						'&from='+start.data[0].lat+','+start.data[0].lon+
						'&to='+end.data[0].lat+','+end.data[0].lon+
                        '&routetype='+routetype+
			//			'&unit=m'+
			//			'&narrativeType=microformat'+
			//			'&enhancedNarrative=true'+
			//			'&generalize=1'+
						'';
    
	Router.config.navigation.routeType = routetype;


	// Call for details on the route
	$.ajax({

		url: jsonURL,
		dataType: 'json',
		data: Router.config.navigation

	}).success(function(result) {

		if (result.info.statuscode === 0) {

			// As MapQuest provide the routing as sequental lat/lngs rather than group
			// we have to reformat it so it is grouped as raw and L.LatLng.

			var	shape = result.route.shape.shapePoints,
				theShape = {
					object: [],
					points: [] },
				sKey = 0,
				pKey = 0,
				shapeCount = shape.length;

			// Shape route
			while (pKey < shapeCount) {

				var pointLat = shape[pKey],
					pointLng = shape[++pKey],
					point = new L.LatLng(pointLat, pointLng);

				theShape.object[sKey] = point;
				theShape.points[sKey] = {
					lat: pointLat,
					lng: pointLng };
					
				++sKey;
				++pKey;
			}

			result.shape = theShape;


			// Do the call back! Do do do de do!

			callback(result);

		} else {

			dialog.alert('<strong>Error with directions</strong><br />'+result.info.messages[0]);
			return false;

		}
	});

};


/*****************************************************************************/


/**
 * Setup Location
 * Takes the location then processes it via `revGeocode()` to give us a full
 * location description
 * @param  {String} locale The location we want to check
 * @return {Object} The full description of a location
 */
Router.setupLocation = function(locale) {

	var returnObj = {};

	if (locale !== undefined && locale) {

		var self	= this,
			info	= null,
			error	= null;

		if (locale !== 'Current Location') {

			info	= self.revGeocode(locale);

			if (info.error !== undefined) {
				error	= info.error;
			}

		} else {

			latlng	= L.LatLng(userLat,userLng);

		}

		returnObj = {
			name:		locale,
			data:		info,
			error:		error
		};

	} else {
		returnObj = {
			error: 'No location given'
		};
	}

	return returnObj;

};


/*****************************************************************************/


/**
 * Set Start Location
 * Sets the name and also all the info about the location
 *
 * @param {string} locale The start location
 */
Router.setStartLoc = function(locale) {
	this._startLoc = this.setupLocation(locale);
};

Router.getStartLoc = function() {
	return this._startLoc;
};


/*****************************************************************************/


/**
 * Set End Location
 * Sets the name and also all the info about the location
 *
 * @param {string} locale The end location
 */
Router.setEndLoc = function(locale) {
	this._endLoc = this.setupLocation(locale);
};

Router.getEndLoc = function() {
	return this._endLoc;
};


/*****************************************************************************/


Router.setRouteType = function(routetype) {
	this._routetype = routetype;
};

Router.getRouteType = function() {
	return this._routetype;
};


/*****************************************************************************/


Router.setDirections = function(directions) {
	this._directions = directions;
};


Router.getDirections = function() {
	return this._directions;
};


/*****************************************************************************/


Router.setMPG = function(mpg) {
    this._mpg = mpg;
};

Router.getMPG = function() {
    return this._mpg;
};


/*****************************************************************************/


Router.setAvgCostPerLitre = function(cost) {
    this._avgCostPerLitre = cost;
};

Router.getAvgCostPerLitre = function() {
    return this._avgCostPerLitre;
};


/*****************************************************************************/


/**
 * Menu Select
 *
 * @param  {string} option The option selected
 * @return {bool}  Returns true if change successful, otherwise false
 */
Router.menuSelect = function(option) {

    var self = this;

    switch(option) {

        case 'route':

            $(Router.config.element.menuPlan).addClass('on');

            $(Router.config.element.menuDirections).removeClass('on');

            $(Router.config.element.directions)
                .animate( {opacity: 0}, self.menuDuration)
                .addClass('hide');
            $(Router.config.element.plan)
                .animate( {opacity: 1}, self.menuDuration)
                .removeClass('hide');

            self.menuArrow(Router.config.menu.arrowPos.plan);

        break;

        case 'directions':

            // Check if there is anything to show before going to that section
            if ($(Router.config.element.directions).html().length > 0) {

                $(Router.config.element.menuPlan).removeClass('on');

                $(Router.config.element.menuDirections).addClass('on');

                $(Router.config.element.plan)
                    .animate( {opacity: 0}, self.menuDuration)
                    .addClass('hide');
                $(Router.config.element.directions)
                    .animate( {opacity: 1}, self.menuDuration)
                    .removeClass('hide');

                self.menuArrow(Router.config.menu.arrowPos.directions);

            } else {
                dialog.alert('You need to confirm your route before viewing directions.');
            }

        break;

        default: return false;
    }

    return true;

};


/*****************************************************************************/


/**
 * Route Menu Arrow
 * @param  {int} moveby The pixel value we need the arrow to move from the left
 * @return {bool} True if effect was achieve, false if nothing was done
 */
Router.menuArrow = function(moveby) {

    var self        = this,
        viewMenu    = $(Router.config.menu.container).find('ul');

    if (moveby === undefined) {

        viewMenu.append('<i class="arrow"></i>');
        viewMenu.find('.arrow').css('left', '65px').css('z-index', '0');

        return true;

    } else {

        viewMenu.find('.arrow').animate({
            left: moveby
        }, self.menuDuration);

        return true;

    }

    return false;

};


/*****************************************************************************/
OpenSource Router
===============================================================================

OpenSource Router is a fork of the original Route Planner project for 
PetrolPrices.com. This version has been built with the intention of making a
modular route planning app that anybody can build on.